import { useState, useEffect, useRef, useCallback } from 'react'
import styles from './canvas.module.css'

import Button from '../Button/Button'

const initialLine = {
  start: null,
  end: null,
  id: null,
  color: 'black',
  middle: null, //middle x y coordinates of the line
  isCollapsed: false,
  circles: [], //array of crossing lines points
}

const NewCanvas = () => {
  const canvasRef = useRef(null)
  const contextRef = useRef(null)
  const animationtRef = useRef(null)

  const [animationIds, setAnimationIds] = useState([])

  const [currentLine, setCurrentLine] = useState(initialLine)

  const [linesArray, setLinesArray] = useState([])

  //to make circle object
  const returnCircle = useCallback(
    ({
      x,
      y,
      color = 'red',
      radius = 10,
      startAngle = 0,
      endAngle = 2 * Math.PI,
      lineId = null,
    }) => ({
      start: { x, y },
      lineId,
      color,
      radius,
      startAngle,
      endAngle,
    }),
    []
  )

  //to drow circle
  const drowCircle = useCallback(
    (item) => {
      const context = contextRef.current

      context.beginPath()
      context.strokeStyle = item.color

      context.moveTo(item.start.x, item.start.y)
      context.arc(
        item.start.x,
        item.start.y,
        item.radius,
        item.startAngle,
        item.endAngle
      )
      context.fillStyle = item.color
      context.fill()
      context.stroke()
    },
    [contextRef]
  )

  //to drow line + circles
  const drowLine = useCallback(
    (item) => {
      const context = contextRef.current
      context.strokeStyle = item.color
      context.beginPath()
      context.moveTo(item.start.x, item.start.y)
      context.lineTo(item.end.x, item.end.y)
      context.lineCap = 'round'
      context.stroke()

      if (item.circles.length > 0 && !item.isCollapsed) {
        item.circles.map(drowCircle)
      }
      if (item.isCollapsed) {
        drowCircle(
          returnCircle({
            x: item.middle.x,
            y: item.middle.y,
            color: 'blue',
            radius: 7,
            lineId: item.id,
          })
        )
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    },
    [contextRef, drowCircle, returnCircle]
  )

  //to clear canvas on reload
  const clearCanvas = useCallback(
    () =>
      contextRef.current.clearRect(
        0,
        0,
        canvasRef.current.width,
        canvasRef.current.height
      ),
    [canvasRef, contextRef]
  )

  //return coordinates object
  const makeCoordinates = useCallback(
    (x, y) => {
      x = x - canvasRef.current.offsetLeft
      y = y - canvasRef.current.offsetTop
      return { x, y }
    },
    [canvasRef]
  )

  //to calculate middle point of the line
  const calculateLineMiddle = useCallback((coord1, coord2) => {
    return Math.floor((coord1 + coord2) / 2)
  }, [])

  //to add coordinates to current line that is drowing
  const canvasClick = useCallback(
    (event) => {
      const { pageX, pageY } = event.nativeEvent
      const coordinates = makeCoordinates(pageX, pageY)

      if (!currentLine.start) {
        setCurrentLine((prev) => ({
          ...prev,
          start: coordinates,
          id: Date.now(),
        }))
      } else {
        const newLine = {
          ...currentLine,
          middle: {
            x: calculateLineMiddle(currentLine.start.x, coordinates.x),
            y: calculateLineMiddle(currentLine.start.y, coordinates.y),
          },
          end: coordinates,
          color: 'black',
        }
        setCurrentLine(initialLine)
        setLinesArray((prev) => [...prev, newLine])
      }
    },
    [makeCoordinates, currentLine, calculateLineMiddle]
  )

  //https://sodocumentation.net/html5-canvas/topic/5017/collisions-and-intersections
  const lineSegmentsIntercept = (function () {
    // function as singleton so that closure can be used

    var v1, v2, v3, cross, u1, u2 // working variable are closed over so they do not need creation
    // each time the function is called. This gives a significant performance boost.
    v1 = { x: null, y: null } // line p0, p1 as vector
    v2 = { x: null, y: null } // line p2, p3 as vector
    v3 = { x: null, y: null } // the line from p0 to p2 as vector

    function lineSegmentsIntercept(p0, p1, p2, p3) {
      v1.x = p1.x - p0.x // line p0, p1 as vector
      v1.y = p1.y - p0.y
      v2.x = p3.x - p2.x // line p2, p3 as vector
      v2.y = p3.y - p2.y
      if ((cross = v1.x * v2.y - v1.y * v2.x) === 0) {
        // cross prod 0 if lines parallel
        return false // no intercept
      }
      v3 = { x: p0.x - p2.x, y: p0.y - p2.y } // the line from p0 to p2 as vector
      u2 = (v1.x * v3.y - v1.y * v3.x) / cross // get unit distance along line p2 p3
      // code point B
      if (u2 >= 0 && u2 <= 1) {
        // is intercept on line p2, p3
        u1 = (v2.x * v3.y - v2.y * v3.x) / cross // get unit distance on line p0, p1;
        if (u1 >= 0 && u1 <= 1) {
          return {
            x: p0.x + v1.x * u1,
            y: p0.y + v1.y * u1,
          }
        }
      }
      return false // no intercept;
      // code point B end
    }
    return lineSegmentsIntercept // return function with closure for optimisation.
  })()

  //find crossing with other lines from whole array
  const compareCrossing = useCallback(
    (currentLine) => {
      const circles = [] //array of circles for current building line

      const { start, end } = currentLine
      if (start && end) {
        //loop for others lines crossing
        for (let line of linesArray) {
          const result = lineSegmentsIntercept(
            currentLine.start,
            currentLine.end,
            line.start,
            line.end
          )
          if (result) {
            circles.push(
              returnCircle({
                x: result.x,
                y: result.y,
                color: 'red',
                radius: 5,
                lineId: currentLine.id,
              })
            )
          }
        }
      }
      return circles
    },
    [linesArray, lineSegmentsIntercept, returnCircle]
  )

  //to drow blue line after first click on canvas
  const canvasMouseHover = useCallback(
    (event) => {
      const { pageX, pageY } = event.nativeEvent
      if (currentLine.start) {
        const newLine = {
          ...currentLine,
          color: 'blue',
          end: makeCoordinates(pageX, pageY),
        }
        newLine.circles = compareCrossing(newLine)
        setCurrentLine(newLine)
      }
    },
    [currentLine, makeCoordinates, compareCrossing, setCurrentLine]
  )

  //remove blue line if pointer is out of canvas
  const canvasMouseLeave = useCallback(() => {
    setCurrentLine(initialLine)
  }, [setCurrentLine])

  const rightButtonClickHandler = useCallback(
    (ev) => {
      ev.preventDefault()
      setCurrentLine(initialLine)
    },
    [setCurrentLine]
  )

  //for collapsing lines
  const coordinateChange = useCallback((coord, middle) => {
    if (coord < middle) {
      return coord + 1
    } else if (coord > middle) {
      return coord - 1
    }

    return coord
  }, [])

  //each line coordinates change animation
  const linesUpdate = useCallback(() => {
    //take only not collapsed lines
    const lines = linesArray.filter((item) => !item.isCollapsed)
    for (let item of lines) {
      if (
        item.start.x === item.end.x &&
        item.end.x === item.middle.x &&
        item.start.y === item.end.y &&
        item.end.y === item.middle.y
      ) {
        item.isCollapsed = true
        setAnimationIds((prev) => prev.filter((id) => id !== item.id))
      } else {
        item.start.x = coordinateChange(item.start.x, item.middle.x)
        item.start.y = coordinateChange(item.start.y, item.middle.y)
        item.end.x = coordinateChange(item.end.x, item.middle.x)
        item.end.y = coordinateChange(item.end.y, item.middle.y)

        if (!animationIds.includes(item.id)) {
          setAnimationIds((prev) => [...prev, item.id])
        }
      }
    }
  }, [linesArray, coordinateChange, setAnimationIds, animationIds])

  //drows all lines after each linesArray change
  const render = useCallback(() => {
    clearCanvas()
    linesArray.forEach(drowLine)
    // console.log('111', linesArray)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [linesArray, currentLine, clearCanvas, drowLine])

  //line collapsing animation loop
  const linesAnimateRun = useCallback(() => {
    const toStart = linesArray.filter((item) => !item.isCollapsed).length
    if (toStart > 0) {
      animationtRef.current = requestAnimationFrame((time) => {
        linesUpdate()
        render()
        linesAnimateRun()
        // console.log('time', time)
      })
    } else {
      cancelAnimationFrame(animationtRef.current)
    }
  }, [animationtRef, linesArray, linesUpdate, render])

  const canvasCollapse = useCallback(() => {
    linesAnimateRun()
  }, [linesAnimateRun])

  useEffect(() => {
    const canvas = canvasRef.current
     // canvas.width = window.innerWidth * 2
    // canvas.height = window.innerHeight * 2
    // canvas.style.width = window.innerWidth + 'px'
    // canvas.style.height = window.innerHeight + 'px'

    canvas.width = 1000
    canvas.height = 500
    canvas.style.width = 1000 + 'px'
    canvas.style.height = 500 + 'px'


    const context = canvas.getContext('2d')
    // context.scale(2, 2)
    context.lineWidth = 1

    contextRef.current = context
  }, [])

  useEffect(() => {
    render()
    console.log(canvasRef.current.parentElement)
  }, [render])

  useEffect(() => {
    if (currentLine.start && currentLine.end) {
      drowLine(currentLine)
    }
  }, [currentLine, drowLine])

  return (
    <div className={styles['canvas-wrapper']}>
      <canvas
        className={styles['canvas']}
        ref={canvasRef}
        onClick={canvasClick}
        onMouseMove={canvasMouseHover}
        onMouseLeave={canvasMouseLeave}
        onContextMenu={rightButtonClickHandler}
      />
      <Button onClick={canvasCollapse} text={'Collapse'}/>
    </div>
  )
}

export default NewCanvas

import React from 'react'

import NewCanvas from './components/NewCanvas/NewCanvas'

const App = () => {
  return (
    <div>
      <NewCanvas />
    </div>
  )
}

export default App
